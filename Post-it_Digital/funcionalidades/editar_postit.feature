#language: pt
#encoding: utf-8

Funcionalidade: Editar Post-it

	# Historia: Editar Post-it

	Para alterar o conteúdo do post-it
	Como usuário comum
	Desejo clicar no meu post-it e digitar as alterações.


	Cenário: O usuário deseja alterar o conteúdo de um post-it especifico
		Dado usuário comum
		Quando clicar no post-it
		Então o sistema deve redireciona-lo para uma pagina de edição daquele post-it

	Cenário: O usuário deseja salvar as alterações feitas no post-it
		Dado usuário comum
		Quando alterar o conteúdo do post-it
			E clicar em salvar
		Então o sistema deve salvar a atualização.

