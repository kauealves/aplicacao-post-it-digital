#language: pt
#encoding: utf-8

Funcionalidade: Pesquisar post-it

	# Historia: Pesquisar por conteudo do post-it

	Para pesquisar meus post-its
	Como usuário comum
	Desejo que os post-its sejam filtrados pelo conteúdo da barra de pesquisa.

	Cenário: O usuário deseja visualizar apenas os post-its com conteúdo da área de busca
		Dado um usuário comum
		Quando escrever uma texto na área de busca
		E clicar em buscar
		Então o sistema deve exibir todos os post-it com aquele conteúdo
