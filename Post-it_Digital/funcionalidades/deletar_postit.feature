#language: pt
#encoding: utf-8

Funcionalidade: Deletar novo post-it
	
	# Historia: Deletar um Post-it

	Para deletar um post-it
	Como usuário comum
	Desejo clicar no botão de deletar post-it

	Cenário: O usuário deseja excluir um Post-it
		Dado usuário comum
		Quando clicar no botão de excluir
		Então o sistema deve retornar a mensagem "Tem certeza disso?"

	Cenário: O usuário deseja realmente deletar seu post-it
		Dado usuário comum 
		Quando o sistema retornar a mensagem "Tem certeza disso?"
			E o usuario clicar em ok
		Então o sistema deve deletar aquele post-it



