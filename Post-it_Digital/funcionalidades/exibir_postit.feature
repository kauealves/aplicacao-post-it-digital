#language: pt
#encoding: utf-8

Funcionalidade: Exibir post-it

	# Historia: Exibir post-it

	Para visualizar meus post-its	
	Como usuário comum
	Desejo ter uma área que mostre todos os post-its

	Cenário: O usuário deseja visualizar todos seus post-its na pagina principal
		Dado usuário comum autenticado
		Quando acessar a pagina principal
		Então o sistema deve exibir todos os post-it
