#language: pt
#encoding: utf-8

Funcionalidade: Criar novo post-it 
	
	# Historia: Criar Post-it

	Para criar um post-it novo
	Como usuário comum do post-it
	Desejo clicar no botão de adicionar post-it
	
	Cenário: O usuário deseja criar um novo Post-it
		Dado usuário comum 
		Quando clicar no botão de adicionar post-it
			E digital uma tag de grupo
		Então o sistema deve criar um objeto post-it
			E redirecionar o usuário para tela de edição daquele post-it
