#language: pt
#encoding: utf-8

Funcionalidade: Mover Post-it

	# Historia: Mover Post-it

	Para mover meu post-it
	Como usuário comum
	Desejo clicar e arrastar meu post-it para posição desejada.

	Cenário: O usuário deseja alterar a posição do post-it
		Dado usuário comum
		Quando clicar no post-it
			E arrastar para posição desejada
		Então o sistema deve setar a nova posição
			E readequar o posicionamento dos demais post-its 

