#language: pt
#encoding: utf-8

Funcionalidade: Filtrar Post-it

	# Historia: Filtrar post-it

	Para pesquisar meus post-its
	Como usuário comum
	Desejo que os post-its sejam filtrados por tag;

	Cenário: O usuário deseja visualizar apenas os post-its da tag selecionada 
		Dado usuário comum
		Quando escrever uma tag na área de busca
		Então o sistema deve exibir todos os post-it com aquela tag
