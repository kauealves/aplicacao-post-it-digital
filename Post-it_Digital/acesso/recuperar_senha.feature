#language: pt
#encoding: utf-8

Funcionalidade: Recuperar Senha

	#História:

	Para recuperar a senha
	Como usuário comum do post-it
	Desejo receber um e-mail com minha senha

	Cenário: O usuário deseja recuperar sua senha no Post-it Digital
		Dado um usuario comum 
		Quando clicar no em recuperar senha
		Então o sistema deve redirecionar o usuário para a pagina de recuperação de senha

	Cenário: O usuário informa seu e-mail cadastrado
		Dado um usuario comum
		Quando prencher o campo de email
			E clicar no botão recuperar minha senha
		Então o sistema envia um e-mail com a nova senha

	Cenário: O usuário informa e-mail não cadastrado
		Dado um usuario comum 
		Quando prencher o cmapo de e-mail
			E clicar no botão recuperar minha senha
		Então o sistema retorna a mensagem "Email não cadastrado"

