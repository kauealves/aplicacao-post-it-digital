#language: pt
#encoding: utf-8

Funcionalidade: Excluir Conta

	# Historia: Excluir uma conta

	Para excluir uma conta do post-it
	Como usuário comum do post-it
	Desejo remover minha conta do sistema.

	Cenário: O usuário deseja excluir sua conta
		Dado usuário comum
		Quando clicar no botão menu
			E Selecionar a opção deletar conta
		Então o sistema deve retornar a mensagem "Tem certeza disso?"

	Cenário: O usuário deseja realmente deletar sua conta 
		Dado usuário comum que recebeu a mensagem de confirmação
		Quando clicar em confirmar
		Então o sistema deve remover todos os seus dados.