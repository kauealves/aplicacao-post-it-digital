#language: pt
#encoding: utf-8

Funcionalidade: Alterar Dados Cadastrais

	# Historia: Alterar Dados Cadastrais

	Para alterar os dados cadastrais
	Como usuario comum do post-it
	Desejo editar meu perfil.
	
	Cenário: O usuario deseja alterar suas informações
		Dado um usuário comum
		Quando clicar no botão menu
			E Selecionar a opção Alterar Dados
		Então o sistema deve redireciona-lo para uma pagina de edição de perfil

	Cenário: O usuario deseja alterar sua senha
		Dado um usuário comum
		Quando alterar o campo de senha
			E clicar em salvar
		Então o sistema deve salvar a alteração.

	Cenário: O usuario deseja alterar seu login
		Dado um usuario comum
		Quando alterar o campo de login
			E clicar em salvar
		Então o sistema deve salvar a alteração.
