#language: pt
#encoding: utf-8

Funcionalidade: Efetuar logout

	# Historia: Efetuar logout

	Para efetuar o logout no Post-it Digital
	Como usuário comum do post-it
	Desejo clicar no menu e selecionar a opção de logout

	Cenário: O usuário deseja efetuar o Logout no Post-it
		Dado usuário comum 
		Quando clicar no botão menu 
			E selecionar logout
		Então o sistema deve encerrar a conexão
			E redirecionar o usuário para a pagina de Login

