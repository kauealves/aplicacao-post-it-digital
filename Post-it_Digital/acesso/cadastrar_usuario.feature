#language: pt
#encoding: utf-8

Funcionalidade: Cadastrar Usuário 

	# Historia: Cadastrar uma conta

	Para me cadastrar no sistema 
	Como usuário comum do post-it
	Desejo obter acesso ao sistema.

	Cenário: O usuário deseja se cadastrar no sistema Post-it Digital:
		Dado um usuário comum
		Quando clicar em cadastrar
		Então o sistema deve redireciona-lo para a página de cadastro.

	Cenário: O usuário informa os dados do formulário corretamente
		Dado um usuário comum
		Quando preencher os campos do formulário
			E clicar em cadastrar
		Então o sistema efetua as verificações 
			E valida os dados.

	Cenário: O usuário informa os dados do formulário incorretamente 
		Dado um usuário comum
		Quando preencher os campos do formulário
		Então o sistema efetua as verificações 
			E retorna erro.

	Cenário: O sistema verifica se o Email já cadastrado 
		Dado um usuário comum
		Quando preencher o campo e-mail 
		Então o sistema verifica se o e-mail está cadastrado  
			E retorna que o o e-mail está cadastrado.
			E bloqueia o botão cadastrar até inserir um e-mail valido

	Cenário: O sistema após validar as informações finaliza o cadastro
		Dado que o um usuário comum tenha preenchido todos os dados
		Quando o sistema receber a requisição de cadastro
		Então deve cadastrar o usuário
			E enviar um e-mail contendo confirmação de cadastro.
