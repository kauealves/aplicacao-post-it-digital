#language: pt
#encoding: utf-8

Funcionalidade: Efetuar login

	# Historia: Efetuar login

	Para efetuar o login no Post-it Digital e ter acessos as suas funcionalidades
	Como usuário comum do post-it
	Desejo informar meus acessos de login e senha.

	Cenário: O usuário deseja efetuar o Login no Post-it
		Dado um usuario comum 
		Quando preencher os campos de usuario e senha
			E os dados estiverem validos
		Então o sistema deve redirecionar o usuário para a pagina principal

	Cenário: O usuário deseja efetuar o Login no Post-it,mas o usuário não está cadastrado
		Dado um usuario comum
		Quando preencher os campos de usuario e senha
			E o usuário não existir no banco de dados
		Então o sistema deve retornar a mensagem de "Usuário não cadastrado"
			E ressaltar a opção de cadastro
	
	Cenário: O usuário deseja efetuar o Login no Post-it, mas a senha está errada
		Dado um usuario comum
		Quando preencher o campo de senha
			E a senha não estiver coerente com o usuário
		Então o sistema deve retornar a mensagem de "Senha Invalida"
			E ressaltar a opção de relembrar senha 


