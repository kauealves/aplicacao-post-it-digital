# Objetivo
  Para usuários de post-its, que não conseguem carregar seus post-its para tudo quanto é lado, o Post-IT Digital é uma aplicação que permite você ver e criar seus post-its em qualquer lugar 😲. Ao contrário do Evernote, nosso produto é para quem tem apego a post-its.

# Lista de Funcionalidades Necessárias:
## Acesso
- Cadastrar conta 
- Deletar conta
- Editar conta
- Efetuar Login
- Efetuar Logout 
- Recuperar Senha

## Funcionalidades
- Criar um Post-it
- Deletar um Post-it
- Editor um Post-it
- Mover um post-it
- Exibir post-it
- Filtrar post-it

## Descrição dos Features

### Cadastrar Usuário

	Essa funcionalidade é responsável pelo recebimento de dados e pelo cadastro de um usuário no Post-it Digital.

### Excluir Conta

	Essa funcionalidade é responsável pela exclusão de uma conta no Post-it Digital.

### Alterar Dados Cadastrais

	Essa funcionalidade é responsável pelas alterações dos campos referentes ao perfil do usuário

### Recuperar Senha
	
	Essa funcionalidade é responsável pelo envio de um email com acessos ao sistema de Post-it

### Efetuar Login

	Essa funcionalidade é responsável por verificar e disponibilizar o acesso ao sistema

### Efetuar Logout

	Essa funcionalidade é responsável por encerrar uma conexão com o sistema do Post-it Digital

### Criar Post-it

	Essa funcionalidade é responsável por criar um objeto post-it e salva-lo no banco.

### Deletar Post-it
	
	Essa funcionalidade é responsável por retirar no sistema os dados de um post-it

### Editar Post-it
	
	Essa funcionalidade é responsável por fazer com que o usuário possa editar seus posts como desejar.

### Mover Post-it

	Essa funcionalidade é responsável por mover um determinado post-it e redefinir os demais a partir da mudança de posição

### Exibir Post-it

	Essa funcionalidade é responsável por exibir os post-its do usuario quando o mesmo acessa a pagina principal

### Filtar Post-it

	Essa funcionalidade é responsável por organizar e filtra os post-its de a acordo com a tag selecionada.

### Pesquisar post-it

	Essa funcionalidade é responsável por buscar no conteúdo do post-it algo que bata com o que o usuario digitou